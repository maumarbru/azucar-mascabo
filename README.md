# Azúcar mascabo 

Un poco de azúcar sintáctica integrada a python.

Este es un repo para charlar, probar y jugar principalmente con la idea de usar funciones como métodos sobrecargando
operadores. Algunas veces se lo llaman _postfix notation_ u otras veces _pipelining_ por el operador `|`.

Postfix se entiende como que la operación se define luego del o los operadores, tipo en vez de
```python
a = [0, 1]
len(a)
```
sería que el operador `.len` se usa después del operando `a`
```python
a.len
# o también
a . len  # ;)
```

Pipelining podríamos decir que es la idea de postfix usando el operador `|`
```python
a | len
# o también
a|len
```
Esto segundo nos recuerda el shell, tipo
```sh
ls | grep azucar
```
donde el resultado del primer proceso se inyecta como entrada del segundo proceso.

## ¿De que se trata todo esto?

En python, los métodos son una manera de ver a la funciones como operadores postfix.
```python
class Int(int):
    @property
    def show(self):
        print(self)

    def show_a_la_cart(self, prefix):
        print(f"{prefix}{self}")
```
Tanto `Int.show` como `Int.show_a_la_cart` son funciones que se utilizan como método de la instancia
```python
>>> s = Int(77)
>>> s.show
77
>>> s.show_a_la_cart("-> ")
-> 77
```
pero se puede utilizar como funciones
```python
>>> Int.show(77)
77
>>> Int.show_a_la_cart(77, "-> ")
-> 77
```

El problema es que tenemos que crear una clase aparte del build-in `int` y convertir los `int`s que nos interesen a
`Int` para gozar de esos método agregados. Esto pasa porque no podemos modificar las "clases" de los tipos build-int.
Pero no pasa sólo con esto, sino que en una gran medida también con una lib que estemos usando. Digamos que no vamos a
andar editando el código fuente o monkipatcheando en general una lib para agregarle métodos a los tipos que nos
interesen. Entonces pasa que caemos en usar la notación prefix de las funciones. 😭

Otra cosa que podría interesarnos es definir uno o varios defaults para los argumentos de los métodos que los
tengan. Para no tener que repetir el default específico. Una forma de hacer es
```python
>>> default_flecha = {"prefix": "-> "}
>>> default_peso = {"prefix": "$ "}
>>> Int.show_a_la_cart(77, **default_flecha)
-> 77
>>> Int.show_a_la_cart(77, **default_peso)
$ 77
```
pero estaría mejor
```python
show_con_flecha = show_a_la_cart(prefix="-> ")
show_con_peso = show_a_la_cart(prefix="$ ")
>>> 77.show_con_flecha
>>> 77.show_con_peso
```
pero hay dos temas acá: no podemos sobrecargar `.` entre un `int` directamente y otro objeto, ni tampoco andar
definiendo así por la vida funciones sin un espacio de nombres o sin algún tipe de poliformismo controlado.

> A veces también pasa que por hacer la clase propia que wrappea a la de la lib, terminamos escondiendo más métodos de
> los que nos gustaría.


## ¿Para qué?
- Evitar el problema de no poder modificar un tipo build-in o de una lib, pero poder extender los verbos (métodos) que
  se aplican a los objetos, inclusive sin cambiarle el comportamiento en el espacio de nombres original (dentro de la
  propia lib, por ejemplo).
- Mayor uniformidad:
  - no estar acotados a mezclar prefix con postfix (llamadas con funciones mezclada con métodos o properties)
  - si no necesito pasar un argumento, usarlo por defecto como una property.
- Poder usar un mismo nombre de método para varios tipos de objeto, sin tener que invocarlos con nombres de módulos
  distintos, dentros de una clase clases o agregarles un prefijo (como en lenguaje C).
- Poder especializar los argumentos por defecto de un método que vamos a usar con frecuencia o un par de veces en un
  espacio de nombres acotado, sin interferir con el comportamiento en otros espacios.


## Inspiraciones
- El múltiple dispatch del lenguaje [Julia](https://docs.julialang.org/en/v1/manual/methods/).
- Recientemente el method call syntax de [Nim](https://nim-lang.org/docs/tut2.html#object-oriented-programming-method-call-syntax).
- Los traits e implementaciones en [Rust](https://doc.rust-lang.org/book/ch10-02-traits.html).
- Los User-Defined Literal ([UDL](https://www.geeksforgeeks.org/user-defined-literals-cpp/)) de C++.

## Espacio de nombres
Las clases justamente son un espacio de nombres, pero si tenemos funciones
que se usan como método sobre la instancia
Uno de los problemas con usar la notación postfix en python con el `.` (punto) es que habría que ir a tocar la clase que
define al objeto cuyo me

En la lib estandar de python tenemos el [singledispatch]() que nos permite definir más de una implementación de
funciones genéricas (poli-mórficas) para el mismo nombre de la función pero para tipos diferentes del primer argumento
de esa función. Esto resuena mucho con el `self` de los métodos.
```python
class Str(str):
    @property
    def lenght(self):
        return len(self)

class Integer(int):
    @property
    def lenght(self):
        return math.log10(self)
```
```python
>>> Str("hola").length
4
>>> Integer(100).length
2
```

```python
@singledispatch
def length(self):  # Default implementation
    return len(self)

@length.register
def _(self: int):  # Spezialization for int
    return math.log10(self)
```
```python
>>> length("hola")
4
>>> length(100)
2
```
pero estaría mejor postfix tipo
```python
>>> "hola" | length
4
>>> 500 | length
2.5
```

## ¿Para qué lo podríamos usar?

### Agregar unidades a las magnitudes
```python
>>> 3.5|Eth
Eth(3.5)
>>> print(3.5|Eth)
3.5_Eth
>>> sarasa.amount() | Eth
Eth(4.1)
```

### REPL
En general cuando hacemos algo interactivamente y vamos aplicando procesos sobre pruebas anteriores, es más
conveniente agregar al final de lo anterior que envolverlo con una función. Es lo clásico que hacemos con `bash`.
Primero vemos como sale. Luego le agregamos algo. Luego a eso le hacemos otra cosa.
```python
>>> a = "1234.5"
>>> float(a)
1234.5
>>> round(float(a))
1235
```
vs
```python
>>> a = "1234.5"
>>> a | float
1234.5
>>> a | float | round
1235
```

### Pipelining

### con el operador directo
```python
(
    Pipe("a list of wordsss")
    | str.split
    | partial(map, len)
    | partial(filter, lambda x: x >= 4)
    | enumerate
    | list
).value
```
```python
(
    Pipes("a list of wordsss")
    / str.split
    @ len
    % (x >= 4)
    / enumerate
    / list
).value
```

### con el operador reverso

```python
(
    "a list of wordsss"
    | split
    | each(len)
    | where(x >= 4)
    | enumerate
    | list
)
```
```python
(
    "a list of wordsss"
    / split
    @ len
    % (x >= 4)
    / enumerate
    / list
)
```

## Ideas similares en python
- https://stackoverflow.com/questions/42965236/how-do-i-define-a-postfix-function-in-python
- https://www.reddit.com/r/Python/comments/rg7i5q/should_python_add_a_pipeline_operator/

