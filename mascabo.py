from functools import update_wrapper, singledispatch


class GenericMethod:
    _convert = staticmethod(singledispatch)

    def __init__(self, wrapped, *args, **kws):
        update_wrapper(self, self._convert(wrapped))
        self.args = args
        self.kws = kws

    def __ror__(self, left):
        return self.__wrapped__(left, *self.args, **self.kws)

    def __call__(self, *args, **kws):
        return self.__class__(self.__wrapped__, *args, **(self.kws | kws))

    __radd__ = __ror__
    __rsub__ = __ror__
    __rmul__ = __ror__
    __rmatmul__ = __ror__
    __rtruediv__ = __ror__
    __rfloordiv__ = __ror__
    __rmod__ = __ror__
    __rdivmod__ = __ror__
    __rpow__ = __ror__
    __rlshift__ = __ror__
    __rrshift__ = __ror__
    __rand__ = __ror__
    __rxor__ = __ror__


genericmethod = GenericMethod


class Method(GenericMethod):
    _convert = staticmethod(lambda f: f)

    def make_generic(self):
        return GenericMethod(self.__wrapped__)

    def become_generic(self):
        update_wrapper(self, singledispatch(self.__wrapped__))


method = Method


class Comparable:
    def __eq__(self, other):
        return lambda x: x == other

    def __ne__(self, other):
        return lambda x: x != other

    def __lt__(self, other):
        return lambda x: x < other

    def __gt__(self, other):
        return lambda x: x > other

    def __le__(self, other):
        return lambda x: x <= other

    def __ge__(self, other):
        return lambda x: x >= other


x = Comparable()


class Pipe:
    def __init__(self, v):
        self.value = v

    def __or__(self, f):
        self.value = f(self.value)
        return self


class Pipes:
    def __init__(self, v):
        self.value = v

    def __truediv__(self, f):
        self.value = f(self.value)
        return self

    def __matmul__(self, f):
        self.value = map(f, self.value)
        return self

    def __mod__(self, f):
        self.value = filter(f, self.value)
        return self
