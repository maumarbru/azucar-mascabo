import math
from mascabo import genericmethod, method, x, Pipe, Pipes


@genericmethod
def length(obj):
    return len(obj)


@length.register
def _(obj: int):
    return math.log10(obj)


@method
def asterix(obj, *, n=1):
    return f'{"*"*n} {obj} {"*"*n}' 


@method
def where(seq, cond):
    return filter(cond, seq)


@method
def each(seq, func):
    for element in seq:
        yield func(element)


@genericmethod
def split(obj):
    return getattr(obj, "split")()


show = method(print)
next_ = method(next)
iter_ = method(iter)
enumerated = method(enumerate)
sorted_ = method(sorted)
rounded = method(round)

as_int = method(int)
as_float = method(float)
as_str = method(str)
as_list = method(list)
as_tuple = method(tuple)
as_dict = method(dict)
as_set = method(set)

to_abs = method(abs)
