from .mascabos import (
    method,
    length as len,
    show as print,
    where as filter,
)

next = method(next)
iter = method(iter)
sorted = method(sorted)
reversed = method(reversed)

int = method(int)
float = method(float)
str = method(str)
list = method(list)
tuple = method(tuple)
dict = method(dict)
set = method(set)

abs = method(abs)
all = method(all)
any = method(any)
hex = method(hex)
# ...
getattr = method(getattr)
setattr = method(setattr)
isinstance = method(isinstance)
