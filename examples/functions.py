import math
from functools import singledispatch


@singledispatch
def length(self):  # Default implementation
    return len(self)


@length.register
def _(self: int):  # Spezialization for int
    return math.log10(self)
