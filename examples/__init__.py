class Int(int):
    @property
    def show(self):
        print(self)

    def show_a_la_cart(self, prefix):
        print(f"{prefix}{self}")
